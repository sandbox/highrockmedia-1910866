Custom Date Formats Examples: 
-------------------- 
A Drupal module

Author: 
-------------------- 
Danny Englander 
http://www.twitter.com/highrockmedia

Description: 
-------------------- 
This is a basic example module of
leveraging custom date formats via a module with drupal. These formats can
then be used for Views, nodes, theming and more. This has come in really
handy for some client projects over the past months so I thought I would
share this. I have no plans to release this as a full module, it will just
stay here in a sandbox.

Installation: 
-------------------- 
- Install as usual, see
http://drupal.org/documentation/install/modules-themes/modules-7 for
further information.


Usage: 
--------------------

